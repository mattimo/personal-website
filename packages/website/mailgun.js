var options = {
    apiKey: "key-dbf7ada2ccf24f9acb8de03954b6eaad",
    domain: "mattmoyles.com"
}
var MailGun = new Mailgun(options);

// In your server code: define a method that the client can call
Meteor.methods({
  sendEmail: function (mailFields) {
      check([mailFields.from, mailFields.subject, mailFields.text, mailFields.html], [String]);

      // Let other method calls from the same client start running,
      // without waiting for the email sending to complete.
      this.unblock();

      MailGun.send({
          to: "mattmoyles@icloud.com",
          from: mailFields.from,
          subject: mailFields.subject,
          text: mailFields.text,
          html: mailFields.html
      });
  }
});
