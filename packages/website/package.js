Package.describe({
  name: "website"
});

Package.onUse(function(api) {
  api.versionsFrom("1.2.1");

  api.use([
    "underscore",
    "templating",
    "tracker",
    "session",
    "ecmascript",
    "spiderable",
    "check",
    "mquandalle:jade@0.4.5",
    "gfk:mailgun-api@1.1.0"
  ], ["client", "server"]);

  api.use([
    "kadira:flow-router@2.7.0",
    "kadira:blaze-layout@2.2.0",
    "twbs:bootstrap@3.3.2",
    "fortawesome:fontawesome@4.2.0",
  ], "client");

  api.addFiles("mailgun.js", "server");

  api.addFiles([
    "router.js",

    // Stylesheets
    //"stylesheets/bootstrap.css",
    /*"stylesheets/bootstrap-theme.css",
    "stylesheets/prettify.css",
    "stylesheets/themify-icons.css",*/

    "stylesheets/styles.css",

    "stylesheets/animate.css",
    "stylesheets/theme.css",
    "stylesheets/color/blue-beige.css",
    "stylesheets/themify-icons.css",

    // Javascript libs
    //"vendor/pace.js",
    //"vendor/bootstrap.js",


    /*"vendor/jquery.appear.js",
    "vendor/jquery.easing.1.3.js",
    "vendor/jquery.localScroll.js",
    "vendor/jquery.scrollTo.js",
    "vendor/jquery.easypiechart.js",
    "vendor/jquery.mb.YTPlayer.js",
    "vendor/jquery.validate.js",
    "vendor/masonry.pkgd.min.js",
    "vendor/owl.carousel.js",
    "vendor/prettify.js",
    "vendor/imagesloaded.pkgd.js",
    "vendor/jquery.waypoints.js",*/

    "vendor/plugins.js",
    "vendor/jquery.LocalScroll.js",
    "vendor/core.js",

    "templates/layouts/main.jade",
    "templates/layouts/main.js"
  ], "client");

  api.addAssets([
    "assets/img/my-location.png",
    "assets/img/photos/avatar-default.jpg",
    "assets/img/photos/it-bg01.jpg",
    "assets/img/photos/it-bg02.png",
    "assets/img/projects/project-square01.jpg",
    "assets/img/projects/project-square02.jpg",
    "assets/img/projects/project-square03.jpg",
    "assets/img/projects/project-big-01.jpg",
    "assets/img/projects/project-big-02.jpg",

    "assets/fonts/themify.eot",
    "assets/fonts/themify.svg",
    "assets/fonts/themify.ttf",
    "assets/fonts/themify.woff",

  ], "client");

});
