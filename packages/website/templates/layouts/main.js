Template["main"].onRendered(function() {
  // Fixes margin from fixed top navbar
  $("body").addClass("white-bg");

  // Initialize theme core script
  Mi.init();
});

Template["main"].events({
  "submit form#contact-form": function(event, template) {
    event.preventDefault();
    const email = template.$("input[name=email]").val();
    const name = template.$("input[name=name]").val();
    const message = template.$("textarea[name=message]").val();

    // Send an email with mailgun
    Meteor.call("sendEmail", {
      from: email,
      subject: "Contact Form - mattmoyles.com - " + name,
      text: message,
      html: message
    }, function(error, result) {
      $formAlert = $('.form-alert');
      $formError = $('.form-error');
      var response;
      $formAlert.hide().html();

      if(!error) {
        response = '<div class="alert alert-success">Done! Thank for your message - You will get you an answer as fast as possible!';
        $formAlert.html(response);
        $formAlert.show();
      } else {
        response = '<div class="alert alert-danger">Ooops... It seems that we have a problem.';
        $formError.html(response);
        $formError.show();
      }
    });


  }
});
